# Parchment

Personal Arch Maintainer -- PArchMaint, a tool to 
keep you sane while maintaining your personal arch linux
installation.

## Background

If you have never asked yourself:
 - Why did I install this package?
 - What was the name of that tool I installed to do X
 - What where those scripts I wrote again and how did they fit together

Then this tool is not for you, move on, no hard feelings here.

If I sparked your interest then use this tool as is, as inspiration or 
as a basis for your own tool then feel free, I would love to hear about 
it but I won't hold you to that.

It all began when I, as most people do, realized that I need to keep
my dotfiles in a repository. I googled other peoples strategies and
choose to use [stow](https://www.gnu.org/software/stow/) for that.

It works reasonably well but only addresses half the problem.
The configuration part. I still have to install the packages when I
install a new computer.

I could export my list of packages with _pacman_ and store that alongside
my dotfiles but I may not want all of them on all my machines.

I need some way to document my workflows and what packages and configurations
are needed for them.

Enter parchment.

## What it is

Parchment is designed to help me group packages and configuration with 
documentation in a structure that makes sense \[to me\]. I have intentionally
adopted a convention that [gitlab](https://gitlab.com) 
(and probably [github](https://github.com)) understands (that is why parchments
are documented with a file named index.md).

My approach is document centric, probably not just a little inspired by emacs
literal configuration style using org-mode. But I apply it on my whole system,
not just my editor. And I use markdown.

I initially resisted my nature to start and never finish a `rust tool` to do
the job so parchment started out as a collection of , pretty bad,
shell scripts. Now I have rewritten most of that in rust since the recursive
nature of this tool and my inablility to stay sane when writing posix compatilble
shell scripts, espeasially in combination.
(If life was supposed to be easy, then why is the temptation of arbitary limitiations
so strong?).
For now I am resisting rewriting _stow_ in rust.

## how it works

In a [dotfiles repository](https://gitlab.com/johlrogge/dotfiles) I create
a number of `parchments`. A `parchment` in it's simplest form is just a
package in a form that would work with [stow](https://www.gnu.org/software/stow/).

A `parchment` can additionally (or alternatively) contain an `index.md` that
documents this `parchment`.

The `index.md` can link to `packages` and other `parchments`, and if it does
those parchments are considered dependecies of this `parchment`.

Parchment links are regular links starting from the root:
`[some parchment](/some_parchment/)`. Package links are anchors under a package
manager wrapper parchment: `[my package](/package_manager#my_package)`.
In this example `/package_manager` is a special parchment for a
`package manager wrapper` that will be used by parchment to install, update,
remove or check status of a package. The `package manager wrapper` parchment
will be an implied dependency of the package installs.

A `package manager wrapper`-parchent is both special and not. It typically
has a `.config/parchment/pm-wrappers/` with a subdirectory with the same
name as the `package manager wrapper` parchment. In the examlpe above
that would be `.config/parchment/pm-wrappers/package_manager`.
This directory contains standardized scripts that `parchment` uses to interact
with the package managers.

These scripts are:
 - install: install a list of packages [see](https://gitlab.com/johlrogge/dotfiles/pacman/.config/parchment/pm-wrappers/pacman/install)
 - status: check the status of a list of packages [see](https://gitlab.com/johlrogge/dotfiles/pacman/.config/parchment/pm-wrappers/pacman/status)

Probably more to come ;)

In addition to this a `package manager wrapper` parchment is like any other `parchment`.
It can depend on parchments, and packages via other `package manager wrappers` that will be
installed before the `package manager wrapper`-scrips are called.

To kick things off you place a special markdown named 
`parchment_host_<hostname>/index.md` in your `dotfiles`-repository. In 
that file you can document your host however you like. The important
part for `parchment` are those links to all the `parchments` that you
want installed on this particular machine. `parchment` will look up
the host name of the current machine when you run it.

It may not be obvious, but `parchment` does not assume any special package
managers. Even _parchments_ are just `packages` that use the `parchment package manager wrapper`
which is like any other `package manager wrapper` except, it cannot refer to other
parchments.

## the plan

Right now I just want to install my new computer but I hope that this
tool can grow into something more powerful.

I want parchment to be able to help you figure out what needs to be
installed, what needs to be updated, especially what is installed that 
should be either documented or _uninstalled_.

In addition make all that documentation work very accessible for you:

like generate manpages,
serve a documentation page for your system.

But right now it _works on my machine_.

## does parchment only work for arch linux?

I'm doing my best not to require arch linux but I'm not testing 
anything else. If there is a situation where compatibility with
other distributions will be really dificult to achieve I will
not loose sleep over requiring arch linux, for now.
