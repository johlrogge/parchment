use {
    super::{ConfigDir, PMWrapperDir},
    directories::ProjectDirs,
    std::path::PathBuf,
};

impl Into<ConfigDir<PathBuf>> for &ProjectDirs {
    fn into(self) -> ConfigDir<PathBuf> {
        ConfigDir(self.config_dir().to_owned())
    }
}

impl Into<PMWrapperDir<PathBuf>> for &ProjectDirs {
    fn into(self) -> PMWrapperDir<PathBuf> {
        let mut pm_wrapper_path = self.config_dir().to_owned();
        pm_wrapper_path.push("pm-wrappers");
        PMWrapperDir(pm_wrapper_path)
    }
}
