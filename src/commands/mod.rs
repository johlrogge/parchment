pub(crate) mod info;
pub(crate) mod install;
pub(crate) mod ls;
pub(crate) mod setup;
pub(crate) mod status;

use crate::config::{ConfigError, PMWrapperDir};
use crate::model::{Link, LinkStatus, PackageManager};
use im::OrdSet;
use std::path::PathBuf;
use structopt::StructOpt;
use thiserror::Error;
use vfs::VfsError;

pub use install::install;
pub use ls::ls;
pub use status::status;

#[derive(Error, Debug)]
pub enum CommandError {
    #[error("host not found {0:?} {1:?}")]
    WrapperNotFound(PackageManager, PathBuf),
    #[error("vfs error {0}")]
    VfsError(#[from] VfsError),
    #[error("configuration error")]
    ConfigError(#[from] ConfigError),
}

pub type Result<T> = std::result::Result<T, CommandError>;

#[derive(StructOpt, Debug)]
#[structopt(about = "Personal arch maintainer")]
pub enum Command {
    /// Install parchment
    Install {
        #[structopt(long)]
        dry_run: bool,
        #[structopt(long)]
        force: bool,
    },

    /// Show parchment status
    Status {},
    /// List parchments
    Ls {},
    /// Display info about parchment and it's configuration
    Info {},
}

pub trait StatusAction
where
    Self: Sized,
{
    fn status(&self, packages: &OrdSet<Link>) -> Vec<LinkStatus>;
    fn load(pm: &PackageManager, pm_wrapper_dir: &PMWrapperDir<PathBuf>) -> Result<Self>;
}

pub trait InstallAction
where
    Self: Sized,
{
    fn load(pm: &PackageManager, pm_wrapper_dir: &PMWrapperDir<PathBuf>) -> Result<Self>;
    fn install(&self, packages: &OrdSet<Link>) -> u16;
}
