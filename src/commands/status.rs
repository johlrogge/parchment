use crate::commands::{CommandError, LinkStatus, StatusAction};
use crate::config::PMWrapperDir;
use crate::model::{Host, Link, LinkOperations, Status};
use crate::output::Output;
use crate::package_manager_wrapper::PackageManagerWrapper;
use crate::parchment::read_parchments_for_host;
use crate::parchment::ParchmentRepository;
use color_eyre::eyre::Result;
use core::fmt::Debug;
use im::{ordset, OrdSet};
use std::path::PathBuf;

pub(crate) fn calculate_statuses<PMW>(
    parchments: OrdSet<Link>,
    pm_wrapper_dir: &PMWrapperDir<PathBuf>,
) -> Result<OrdSet<LinkStatus>>
where
    PMW: StatusAction,
{
    parchments
        .iter()
        .links_by_package_manager()
        .iter()
        .try_fold(ordset![], |acc, (package_manager, ps)| {
            match PMW::load(package_manager, &pm_wrapper_dir) {
                Ok(pm) => Ok(acc.union(pm.status(ps).into())),
                Err(CommandError::WrapperNotFound(pm, _)) => Ok(
                    acc.union(ps
                    .iter()
                    .map(|p| LinkStatus(p.clone(), Status::MissingPm(pm.clone())))
                    .collect())),
                Err(e) => Err(e)?,
            }
        })
}

#[tracing::instrument]
pub fn status<'a, PR, O>(
    dotfiles_root: PR,
    host: &Host,
    pm_wrapper_dir: &PMWrapperDir<PathBuf>,
) -> Result<&'a str>
where
    PR: ParchmentRepository + Debug,
    O: Output + Debug,
{
    let (host_parchments, _dependencies) = read_parchments_for_host(&host, &dotfiles_root)?;
    let link_statuses =
        calculate_statuses::<PackageManagerWrapper>(host_parchments, &pm_wrapper_dir)?;
    for LinkStatus(Link(_pm, pkg), status) in link_statuses.iter() {
        O::text(&format!("{}: {:?}", pkg, status));
        O::new_line();
    }
    Ok("status")
}

#[cfg(test)]
mod tests {
    use crate::commands::status::{calculate_statuses, LinkStatus, OrdSet};
    use crate::commands::StatusAction;
    use crate::config::PMWrapperDir;
    use crate::model::{Link, PackageManager, Status, PARCHMENT_PM};
    use im::ordset;
    use std::path::PathBuf;

    struct StubStatusAction();
    impl StatusAction for StubStatusAction {
        fn status(&self, packages: &OrdSet<Link>) -> Vec<LinkStatus> {
            packages
                .into_iter()
                .map(|p| {
                    println!("{}", p.to_string());
                    LinkStatus(p.to_owned(), Status::Installed)
                })
                .collect()
        }

        fn load(
            pm: &PackageManager,
            _config: &PMWrapperDir<PathBuf>,
        ) -> crate::commands::Result<Self> {
            match pm {
                PackageManager(pm_name) if pm_name == PARCHMENT_PM => Ok(Self()),
                _ => Err(crate::commands::CommandError::WrapperNotFound(
                    PackageManager(pm.0.to_owned()),
                    PathBuf::new(),
                )),
            }
        }
    }

    #[test]
    fn calculate_statuses_for_zero_links_returns_zero_statuses() {
        let result =
            calculate_statuses::<StubStatusAction>(ordset!(), &PMWrapperDir(PathBuf::default()))
                .unwrap();
        assert_eq!(result, ordset!());
    }

    #[test]
    fn calculate_statuses_for_one_link_returns_one_status() {
        let result = calculate_statuses::<StubStatusAction>(
            ordset!(Link::new("single_parchment".to_owned())),
            &PMWrapperDir(PathBuf::default()),
        )
        .unwrap();
        assert_eq!(
            result,
            ordset!(LinkStatus(
                Link::new("single_parchment".to_owned()),
                Status::Installed
            ))
        );
    }

    #[test]
    fn calculate_statuses_for_one_link_no_package_manager_installed() {
        let result = calculate_statuses::<StubStatusAction>(
            ordset!(Link::package_link(
                "missing_pm".to_owned(),
                "single_parchment".to_owned()
            )),
            &PMWrapperDir(PathBuf::default()),
        )
        .unwrap();
        assert_eq!(
            result,
            ordset!(
                LinkStatus(
                Link::package_link("missing_pm".to_owned(), "single_parchment".to_owned()),
                Status::MissingPm(PackageManager("missing_pm".to_owned()))
            ))
        );
    }
}
