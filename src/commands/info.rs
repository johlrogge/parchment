use crate::model::Host;

use {
    crate::{
        config::{ConfigDir, ConfigLocation, PMWrapperDir},
        output::Output,
    },
    color_eyre::eyre::Result,
    core::fmt::Debug,
    crossterm::style::Color::*,
    minimad::{OwningTemplateExpander, TextTemplate},
    std::path::PathBuf,
    termimad::*,
};

static INFO_TEMPLATE: &str = r#"
# Parchment Info
## directories
|:-:|:-:|
|**name**|**value**|
|-:|:-|
${directory-rows
|*${directory-name}*|`${directory-value}`|
}
|-|-|
## config
**config-location**: `${config-location}` exits [*${config-exists}*]
|:-:|:-:|
|**key**|**value**|
|-:|:-|
${config-rows
|*${config-name}*|`${config-value}`|
}
|-|-|  
"#;

#[tracing::instrument]
pub fn info<'a, O>(
    config_location: &ConfigLocation<PathBuf>,
    pm_wrapper_dir: &PMWrapperDir<PathBuf>,
) -> Result<&'a str>
where
    O: Output + Debug,
{
    let mut expander = OwningTemplateExpander::new();
    let ConfigLocation(ConfigDir(config_dir)) = config_location.clone();
    let PMWrapperDir(pm_wrapper_path) = pm_wrapper_dir.clone();

    expander
        .set(
            "config-location",
            format!("{}", config_location.to_string()),
        )
        .set(
            "config-exists",
            format!(
                "{}",
                if config_location.exists() {
                    "✓"
                } else {
                    "❌using defaults"
                }
            ),
        );

    expander
        .sub("directory-rows")
        .set("directory-name", "config")
        .set(
            "directory-value",
            format!("{}", config_dir.to_string_lossy()),
        );

    expander
        .sub("directory-rows")
        .set("directory-name", "pm-wrappers")
        .set(
            "directory-value",
            format!("{}", pm_wrapper_path.to_string_lossy()),
        );

    let config = config_location.load()?;

    expander
        .sub("config-rows")
        .set("config-name", "host")
        .set("config-value", format!("{}", Host::default()));
    expander
        .sub("config-rows")
        .set("config-name", "dotfiles")
        .set("config-value", format!("{}", config.dotfiles));
    let skin = make_skin();
    let config_template = TextTemplate::from(INFO_TEMPLATE);
    let text = expander.expand(&config_template);
    let (width, _) = terminal_size();
    let fmt_text = FmtText::from_text(&skin, text, Some(width as usize));
    print!("{}", fmt_text);
    Ok("")
}

fn make_skin() -> MadSkin {
    let mut skin = MadSkin::default();
    skin.set_headers_fg(AnsiValue(178));
    skin.headers[2].set_fg(gray(22));
    skin.bold.set_fg(Yellow);
    skin.italic.set_fg(Magenta);
    skin.scrollbar.thumb.set_fg(AnsiValue(178));
    skin
}
