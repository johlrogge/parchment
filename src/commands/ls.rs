use crate::model::{Host, Link};
use crate::output::Output;
use crate::parchment::read_parchments_for_host;
use crate::parchment::ParchmentRepository;
use color_eyre::eyre::Result;
use core::fmt::Debug;

#[tracing::instrument]
pub fn ls<'a, PR, O>(host: &Host, dotfiles_root: PR) -> Result<&'a str>
where
    PR: ParchmentRepository + Debug,
    O: Output + Debug,
{
    let (host_parchments, _dependencies) = read_parchments_for_host(&host, &dotfiles_root)?;
    for Link(pm, pkg) in host_parchments.iter() {
        O::text(&format!("{}: {}", pm.0, pkg));
        O::new_line();
    }
    Ok("ls")
}
