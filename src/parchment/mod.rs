use crate::config::host_parchment_link;
use crate::model::{Dependency, Host, Link, PackageManager, PARCHMENT_PM};
use im::{ordset, OrdSet};
use std::fmt::Debug;
use std::str::FromStr;
use thiserror::Error;
use tracing::{event, span, Level};

#[macro_use]
pub(crate) mod vfs;
mod pulldown;

#[derive(Debug, Clone)]
pub struct Parchment(pub Link);

#[derive(Error, Debug)]
pub enum ParchmentError {
    #[error("configuration error: {0}")]
    ConfigError(#[from] crate::config::ConfigError),
    #[error("error resolving link: {0}")]
    ResolveError(#[from] link_resolution::ResolveError),
    #[error("VfsError: {0}")]
    IoError(String),
}

impl FromStr for Link {
    type Err = ParchmentError;
    fn from_str(s: &str) -> std::result::Result<Self, <Self as std::str::FromStr>::Err> {
        Ok(Link::new(s.to_owned()))
    }
}

type Result<T> = std::result::Result<T, ParchmentError>;

type LinksAndDependencies = (OrdSet<Link>, OrdSet<Dependency>);

/// Read all parchments for the given host
pub fn read_parchments_for_host<'a, PR>(
    host: &Host,
    dotfiles: &PR,
) -> Result<(OrdSet<Link>, OrdSet<Dependency>)>
where
    PR: ParchmentRepository + Debug,
{
    let host_parchment = host_parchment_link(host)?;
    event!(Level::TRACE, "resolving host parchment {}", host_parchment);
    let links: OrdSet<Link> = ordset![];
    let dependencies: OrdSet<Dependency> = ordset![];
    let links_and_dependencies =
        parse_parchments_from_link(&links, &dependencies, &host_parchment, dotfiles)?;

    Ok(links_and_dependencies)
}

pub fn parse_parchments_from_links<PR>(
    found_so_far: &OrdSet<Link>,
    dependencies_so_far: &OrdSet<Dependency>,
    links: OrdSet<Link>,
    dotfiles: &PR,
) -> Result<LinksAndDependencies>
where
    PR: ParchmentRepository + Debug,
{
    span!(Level::TRACE, "parse_parchments_from_links").in_scope(|| {
        links.iter().try_fold(
            (found_so_far.clone(), dependencies_so_far.clone()),
            |acc, curr| {
                event!(Level::TRACE, "parchment: {}", curr);
                match (acc, curr) {
                    ((acc_found_links, acc_found_deps), curr) => parse_parchments_from_link(
                        &acc_found_links,
                        &acc_found_deps,
                        curr,
                        dotfiles,
                    )
                    .map(|(links, deps)| (links + acc_found_links, deps + acc_found_deps)),
                }
            },
        )
    })
}

pub trait ParchmentRepository {
    fn resolve_link(&self, link: &Link) -> Result<OrdSet<Link>>;
}

pub fn parse_parchments_from_link<PR>(
    found_so_far: &OrdSet<Link>,
    dependencies_so_far: &OrdSet<Dependency>,
    link: &Link,
    dotfiles: &PR,
) -> Result<LinksAndDependencies>
where
    PR: ParchmentRepository + Debug,
{
    span!(Level::TRACE,"links for ", %link).in_scope(|| {
        let links = dotfiles.resolve_link(&link)?.update(link.clone());
        let dependencies: OrdSet<Dependency> = links
            .iter()
            .cloned()
            .map(|d| link.to_owned().depends_on(d))
            .collect();

        fn add_pm_dependency(dependency: &Dependency) -> OrdSet<Dependency> {
            let Dependency {
                from: Link(PackageManager(from_pm), ..),
                to: Link(PackageManager(to_pm), ..),
            } = dependency.clone();
            ordset!(
                dependency.clone(),
                dependency.clone().from.depends_on(Link::new(from_pm)),
                dependency.clone().to.depends_on(Link::new(to_pm))
            )
        }

        let dependencies: OrdSet<Dependency> = dependencies
            .iter()
            .flat_map(add_pm_dependency)
            .filter(|dep| dep.from != dep.to)
            .collect();
        let dependencies_so_far = dependencies_so_far.to_owned() + dependencies.clone();

        let links: OrdSet<Link> = dependencies
            .iter()
            .map(|Dependency { to, .. }| to.clone())
            .collect();
        let found_so_far = found_so_far.clone() + ordset!(link.clone());
        let links = links.relative_complement(found_so_far.clone());
        parse_parchments_from_links::<PR>(&found_so_far, &dependencies_so_far, links, &dotfiles)
            .map(|(links, deps)| (found_so_far.clone() + links, dependencies_so_far + deps))
    })
}

#[cfg(test)]
mod parse_parchments_from_link {
    use super::{parse_parchments_from_link, Link, LinksAndDependencies, ParchmentError};
    use crate::parchment::ParchmentRepository;
    use core::fmt::Debug;
    use im::ordset;
    use tracing_test::traced_test;
    fn act<PR>(link: &Link, dotfiles: &PR) -> Result<LinksAndDependencies, ParchmentError>
    where
        PR: ParchmentRepository + Debug,
    {
        parse_parchments_from_link(&ordset![], &ordset![], link, dotfiles)
    }

    #[test]
    fn finds_self_when_no_index_exists() {
        let leaf = Link::new("leaf");
        let pm_link = Link::new("parchment");
        let dotfiles = setup_files! {};
        let (resolved, dependencies) = act(&leaf, &dotfiles).unwrap();
        assert_eq!(ordset![leaf.clone(), pm_link.clone()], resolved);
        assert_eq!(ordset![leaf.depends_on(pm_link)], dependencies);
    }

    #[test]
    fn finds_direct_dependency() -> ::vfs::error::VfsResult<()> {
        let dotfiles = setup_files! {
                root/"index.md": b"[leaf](/leaf)"
                leaf/"index.md": b"leaf"
        };
        let leaf = Link::new("leaf");
        let root = Link::new("root");
        let parch_pm_link = Link::new("parchment");

        let (resolved, dependencies) = act(&root, &dotfiles).unwrap();
        assert_eq!(
            ordset![root.clone(), leaf.clone(), parch_pm_link.clone()],
            resolved
        );
        assert_eq!(
            ordset![
                root.clone().depends_on(leaf.clone()),
                leaf.clone().depends_on(parch_pm_link.clone()),
                root.clone().depends_on(parch_pm_link)
            ],
            dependencies
        );
        Ok(())
    }

    #[test]
    fn finds_indirect_dependency() -> ::vfs::error::VfsResult<()> {
        let dotfiles = setup_files! {
            root/"index.md"    : b"[direct](/direct)"
            direct/"index.md"  : b"[indirect](/indirect)"
            indirect/"index.md": b"indirect"
        };
        let indirect = Link::new("indirect");
        let direct = Link::new("direct");
        let root = Link::new("root");
        let pm_link = Link::new("parchment");
        let (resolved, dependencies) = act(&root, &dotfiles).unwrap();
        assert_eq!(
            ordset![
                root.clone(),
                direct.clone(),
                indirect.clone(),
                pm_link.clone()
            ],
            resolved
        );
        assert_eq!(
            ordset![
                root.clone().depends_on(direct.clone()),
                root.depends_on(pm_link.clone()),
                direct.clone().depends_on(indirect.clone()),
                direct.depends_on(pm_link.clone()),
                indirect.depends_on(pm_link)
            ],
            dependencies
        );
        Ok(())
    }

    #[test]
    fn finds_circular_dependencies() -> ::vfs::error::VfsResult<()> {
        let dotfiles = setup_files! {
            root/"index.md"           : b"[direct](/direct)"
            direct/"index.md"         : b"[back_to_direct](/back_to_direct)"
            back_to_direct/"index.md" : b"[direct](/direct)"
        };
        let back_to_direct = Link::new("back_to_direct");
        let direct = Link::new("direct");
        let root = Link::new("root");
        let parchment_link = Link::new("parchment");
        let (resolved, dependencies) = act(&root, &dotfiles).unwrap();
        assert_eq!(
            ordset![
                root.clone(),
                direct.clone(),
                back_to_direct.clone(),
                parchment_link.clone()
            ],
            resolved
        );
        assert_eq!(
            ordset![
                root.clone().depends_on(direct.clone()),
                direct.clone().depends_on(back_to_direct.clone()),
                back_to_direct.clone().depends_on(direct.clone()),
                root.depends_on(parchment_link.clone()),
                direct.depends_on(parchment_link.clone()),
                back_to_direct.depends_on(parchment_link)
            ],
            dependencies
        );
        Ok(())
    }

    #[traced_test]
    #[test]
    fn finds_package_links() -> ::vfs::error::VfsResult<()> {
        let dotfiles = setup_files! {
                root/"index.md": b"
 [some package](/some_pm#some_package)
 "
        };
        let root = Link::new("root");
        let some_package = Link::package_link("some_pm", "some_package");
        let some_pm = Link::new("some_pm");
        let parchment_pm = Link::new("parchment");

        let (resolved, dependencies) = act(&root, &dotfiles).unwrap();
        assert_eq!(
            ordset![
                root.clone(),
                some_package.clone(),
                some_pm.clone(),
                parchment_pm.clone()
            ],
            resolved
        );
        assert_eq!(
            ordset![
                root.clone().depends_on(some_package.clone()),
                root.clone().depends_on(parchment_pm.clone()),
                some_package.clone().depends_on(some_pm.clone()),
                some_pm.clone().depends_on(parchment_pm)
            ],
            dependencies
        );
        Ok(())
    }

    #[traced_test]
    #[test]
    fn ignores_http_links() -> ::vfs::error::VfsResult<()> {
        let dotfiles = setup_files! {
                root/"index.md": b"
 [some external link](http://site.com#some_anchor)
 "
        };
        let root = Link::new("root");
        let parchment_pm = Link::new("parchment");

        let (resolved, dependencies) = act(&root, &dotfiles).unwrap();
        assert_eq!(ordset![root.clone(), parchment_pm.clone()], resolved);
        assert_eq!(
            ordset![root.clone().depends_on(parchment_pm.clone())],
            dependencies
        );
        Ok(())
    }
}

pub mod link_resolution {
    use crate::parchment::Link;
    use crate::parchment::PackageManager;
    use crate::parchment::PARCHMENT_PM;
    use thiserror::Error;
    use tracing::{event, Level};
    use vfs::{VfsError, VfsPath};

    #[derive(Debug, Error)]
    pub enum ResolveError {
        #[error("could not create path from link")]
        PathCreationError(#[from] std::path::StripPrefixError),
        #[error("cannot resolve package link {}", .0)]
        CannotResolvePackageLink(Link),
        #[error("vfs error")]
        VfsError(#[from] VfsError),
    }
    pub(super) fn find_link_target(
        from_root: &VfsPath,
        link: &Link,
    ) -> Result<VfsPath, ResolveError> {
        event!(Level::TRACE, "find_link_target");
        match link {
            Link(pm, parchment) if pm == &PackageManager(PARCHMENT_PM.to_owned()) => {
                let target = &parchment;
                let parchment_base = from_root;
                Ok(parchment_base.join(target)?)
            }
            _ => Err(ResolveError::CannotResolvePackageLink(link.clone())),
        }
    }

    #[cfg(test)]
    mod resolve_links {
        use crate::parchment::link_resolution::find_link_target;
        use crate::parchment::Link;
        use std::str::FromStr;
        use vfs::{MemoryFS, VfsPath};

        #[test]
        fn resolve_link_path() {
            let root: VfsPath = MemoryFS::new().into();
            let root = root.join("dotfiles").unwrap();
            let actual = find_link_target(&root, &Link::from_str("thing").unwrap()).unwrap();
            let expected = root.join("thing").unwrap();
            assert_eq!(expected.as_str(), actual.as_str());
        }
    }
}
