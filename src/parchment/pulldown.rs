use crate::model::Link;
use im::OrdSet;
use pulldown_cmark::{Event, Parser, Tag};
use tracing::{event, Level};

fn link_from_markdown_link(md_link: String) -> Link {
    let ln = md_link
        .strip_prefix("/")
        .expect("link target did not start with '/'");

    match &ln.split_once("#") {
        Some((pm, pkg)) => Link::package_link(pm, pkg),
        None => Link::new(ln),
    }
}

pub fn extract_links<'a>(document: &'a str) -> OrdSet<Link> {
    event!(Level::TRACE, "extract_links");
    Parser::new_ext(document, pulldown_cmark::Options::empty())
        .filter_map(|event| match event {
            Event::Start(Tag::Link(_, target, ..)) => {
                event!(Level::TRACE, "found link {}", target);
                let target = target.into_string();
                if target.starts_with("/") {
                    Some(link_from_markdown_link(target))
                } else {
                    None
                }
            }
            _ => None,
        })
        .collect()
}

#[cfg(test)]
mod extract_links {
    use super::extract_links;
    use crate::parchment::Link;
    use im::{ordset, OrdSet};

    #[test]
    fn extracts_no_links_from_empty_document() {
        let actual = extract_links("");
        let expected: OrdSet<Link> = ordset![];
        assert_eq!(expected, actual);
    }

    #[test]
    fn extracts_one_link_without_title() {
        let actual = extract_links("[](/a_link) ");
        let expected: OrdSet<Link> = ordset![Link::new("a_link")];
        assert_eq!(expected, actual);
    }

    #[test]
    fn extracts_one_package_link_without_title() {
        let actual = extract_links("[](/a_pm#a_package) ");
        let expected: OrdSet<Link> = ordset![Link::package_link("a_pm", "a_package")];
        assert_eq!(expected, actual);
    }
}
